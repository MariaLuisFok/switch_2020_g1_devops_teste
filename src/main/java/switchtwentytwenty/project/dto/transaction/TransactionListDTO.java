package switchtwentytwenty.project.dto.transaction;

import lombok.Getter;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class TransactionListDTO extends RepresentationModel<TransactionListDTO> {
    @Getter
    private final List<TransactionOutputDTO> transactionList;

    public TransactionListDTO(List<TransactionOutputDTO> transactionList) {
        this.transactionList = transactionList;
    }
}

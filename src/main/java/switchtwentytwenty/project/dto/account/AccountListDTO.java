package switchtwentytwenty.project.dto.account;


import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;
import switchtwentytwenty.project.domain.model.account.CashAccount;
import switchtwentytwenty.project.domain.model.account.PersonalBankAccount;

import java.util.List;
import java.util.Objects;

@NoArgsConstructor
public class AccountListDTO extends RepresentationModel<AccountListDTO> {

    @Getter
    private List<CashAccount> cashAccountList;

    @Getter
    private List<PersonalBankAccount> personalBankAccount;


    public AccountListDTO(List<PersonalBankAccount> personalBankAccount, List<CashAccount> cashAccount) {
        this.cashAccountList = cashAccount;
        this.personalBankAccount = personalBankAccount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AccountListDTO)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AccountListDTO that = (AccountListDTO) o;
        return Objects.equals(getCashAccountList(), that.getCashAccountList()) && Objects.equals(getPersonalBankAccount(), that.getPersonalBankAccount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getCashAccountList(), getPersonalBankAccount());
    }
}

import React from "react";
import "./CreateRelationship.styles.css";
import TextField from '@material-ui/core/TextField';
import {makeStyles} from '@material-ui/core/styles';
import {FormControl, FormLabel, InputLabel, MenuItem, Select} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    root: {
        '& .MuiTextField-root': {
            top: 5,
            margin: theme.spacing(1),
            width: '35ch',
            fontSize: 20,
            align: 'center'
        },
    },
}));

function CreateRelationshipForm({parentSubmitHandler}) {
    const inputMainUserId = React.useRef("");
    const inputOtherUserId = React.useRef("");
    const inputRelationshipType = React.useRef("");

    const classes = useStyles();
    const margin = {
        paddingTop: '50px',
        paddingBottom: '20px',
    }
    const dropdown = {
        top: 20,
        width: '35ch',

    }

    let submitHandler = e => {
        e.preventDefault()
        const data = {
            mainUserId: inputMainUserId.current.value,
            otherUserId: inputOtherUserId.current.value,
            relationshipType: inputRelationshipType.current.value
        }
        console.log(data)

        parentSubmitHandler(data);
    }

    return (
        <div align='center' style={margin}>
            <h1>Create a new relationship</h1>
            <form onSubmit={submitHandler} className={classes.root}>
                <div>
                    <TextField required label="Main User Email:" inputRef={inputMainUserId}/>
                </div>
                <br/>
                <div>
                    <TextField required label="Other User Email:" inputRef={inputOtherUserId}/>
                </div>
                <br/>
                <div>
                    <FormControl style={dropdown}>
                        <InputLabel>RelationshipType *</InputLabel>
                        <Select defaultValue="" inputRef={inputRelationshipType} required>RelationshipType>
                            <MenuItem value={1}>Spouse</MenuItem>
                            <MenuItem value={2}>Partner</MenuItem>
                            <MenuItem value={3}>Parent</MenuItem>
                            <MenuItem value={4}>Child</MenuItem>
                            <MenuItem value={5}>Sibling</MenuItem>
                            <MenuItem value={6}>Grandparent</MenuItem>
                            <MenuItem value={7}>Grandchild</MenuItem>
                            <MenuItem value={8}>Uncle/Aunt</MenuItem>
                            <MenuItem value={9}>Nephew/Nice</MenuItem>
                            <MenuItem value={10}>Cousin</MenuItem>
                        </Select>
                    </FormControl>
                </div>
                <br/>
                <br/>
                <br/>
                <button type="submit" id="button">Create</button>
            </form>
        </div>
    )
}

export default CreateRelationshipForm;

package switchtwentytwenty.project.applicationservices.implservices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import switchtwentytwenty.project.applicationservices.iservices.ITransactionService;
import switchtwentytwenty.project.assemblers.TransactionAssembler;
import switchtwentytwenty.project.domain.domainservices.TransactionDomainService;
import switchtwentytwenty.project.domain.exceptions.InvalidAmountException;
import switchtwentytwenty.project.domain.factories.PaymentFactory;
import switchtwentytwenty.project.domain.factories.TransferFactory;
import switchtwentytwenty.project.domain.model.category.BaseCategory;
import switchtwentytwenty.project.domain.model.shared.*;
import switchtwentytwenty.project.domain.model.transaction.Payment;
import switchtwentytwenty.project.domain.model.transaction.Transaction;
import switchtwentytwenty.project.dto.account.AccountBalanceDTO;
import switchtwentytwenty.project.dto.account.AccountBalanceMapper;
import switchtwentytwenty.project.dto.transaction.PaymentInputDTO;
import switchtwentytwenty.project.dto.transaction.PaymentVOs;
import switchtwentytwenty.project.domain.model.transaction.Transfer;
import switchtwentytwenty.project.dto.transaction.*;
import switchtwentytwenty.project.repositories.AccountRepository;
import switchtwentytwenty.project.repositories.CategoryRepository;
import switchtwentytwenty.project.repositories.TransactionRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionService implements ITransactionService {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    private TransactionRepository transactionRepository;
    @Autowired
    private TransactionAssembler transactionAssembler;

    @Override
    @Transactional
    public TransactionOutputDTO registerPayment(PaymentInputDTO paymentInputDTO, long accountId) {
        existsAccount(accountId);
        PaymentVOs paymentVOs = transactionAssembler.toDomain(paymentInputDTO, accountId);
        CategoryName categoryName = getCategoryName(paymentVOs.getCategoryId());
        Payment aPayment = PaymentFactory.buildPayment(paymentVOs);
        Balance prePaymentBalance = getBalance(aPayment);
        validatePayment(prePaymentBalance, aPayment.getAmount());
        Balance afterPaymentBalance = getBalanceAfterTransaction(prePaymentBalance, aPayment);
        Payment savedPayment = (Payment) transactionRepository.save(aPayment);
        return transactionAssembler.toOutputDTO(savedPayment, categoryName, afterPaymentBalance);
    }

    @Override
    @Transactional
    public TransactionOutputDTO transfer(TransferInputDTO transferInputDTO, long originAccountId, long destinationAccountId) {
        existsAccount(originAccountId);
        TransferVOs transferVOs = transactionAssembler.toDomain(transferInputDTO, originAccountId, destinationAccountId);
        CategoryName categoryName = getCategoryName(transferVOs.getCategoryId());
        Transfer aTransfer = TransferFactory.buildTransfer(transferVOs);
        Transfer savedTransfer = (Transfer) transactionRepository.save(aTransfer);
        return transactionAssembler.toOutputDTO(savedTransfer, categoryName);
    }

    private CategoryName getCategoryName(CategoryId categoryId) {
        BaseCategory aCategory = getById(categoryId);
        return aCategory.getName();
    }

    private BaseCategory getById(CategoryId categoryId) {
        return categoryRepository.getCategory(categoryId);
    }

    private void existsAccount(long accountId) {
        accountRepository.existsAccount(accountId);
    }

    public Balance getBalance(Payment aPayment) {
        List<Transaction> transactions = transactionRepository.findAllByAccountId(aPayment.getAccountId());
        InitialAmountValue initialAmountValue = accountRepository.getByAccountId(aPayment.getAccountId());
        return calculateBalance(transactions, initialAmountValue);
    }

    private Balance calculateBalance(List<Transaction> transactions, InitialAmountValue initialAmount) {
        double balance = initialAmount.getAmount();
        for (Transaction transaction : transactions) {
            balance += transaction.getAmount().getAmount();
        }
        return new Balance(balance);
    }

    private void validatePayment(Balance balance, TransactionAmount amount) {
        if (balance.getAmount() + amount.getAmount() <= 0) {
            throw new InvalidAmountException("The transaction cannot be completed. There's not enough money in your account.");
        }
    }

    private Balance getBalanceAfterTransaction(Balance preTransactionBalance, Transaction transaction) {
        double balance = preTransactionBalance.getAmount() + transaction.getAmount().getAmount();
        return new Balance(balance);
    }

    @Override
    public AccountBalanceDTO getAccountBalance(long aAccountId){
        existsAccount(aAccountId);
        AccountId accountId = new AccountId(aAccountId);
        List<Transaction> transactions = transactionRepository.findAllByAccountId(accountId);
        InitialAmountValue initialAmountValue = accountRepository.getByAccountId(accountId);
        Balance accountBalance = calculateBalance(transactions, initialAmountValue);

        AccountBalanceMapper mapper = new AccountBalanceMapper();

        return mapper.toDTO(accountBalance);
    }

    @Override
    public TransactionListDTO getAccountTransactionsBetweenDates(long accountId, DateRangeDTO dateRangeDTO) {
        List<TransactionOutputDTO> listToReturn = new ArrayList<>();
        AccountId accountIdVO = new AccountId(accountId);
        List<Transaction> transactionList = transactionRepository.findAllByAccountId(accountIdVO);
        String startDate = dateRangeDTO.getStartDate();
        String endDate = dateRangeDTO.getEndDate();
        List<Transaction> transactionsBetweenDates = TransactionDomainService.getTransactionsBetweenDates(transactionList, startDate, endDate);

        for (Transaction transaction : transactionsBetweenDates) {
            CategoryId categoryId = transaction.getCategoryId();
            CategoryName categoryName = getCategoryName(categoryId);
            TransactionOutputDTO dto = TransactionsMapper.mapTransaction(transaction, categoryName);
            listToReturn.add(dto);
        }

        return new TransactionListDTO(listToReturn);
    }
}
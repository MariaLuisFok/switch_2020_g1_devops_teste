package switchtwentytwenty.project.dto.transaction;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TransactionOutputDTOTest {

    @Test
    void createTransactionOutputDTOSuccessfully() {
        long transactionId = 123;
        double amount = 34.45;
        String currency = "EUR";
        String description = "Transaction";
        String date = "12/02/2020 15:56";
        String categoryName = "Groceries";

        TransactionOutputDTO transactionOutputDTO = new TransactionOutputDTO(transactionId, amount, currency, description, date, categoryName);

        assertNotNull(transactionOutputDTO);
        assertEquals(transactionId, transactionOutputDTO.getTransactionId());
        assertEquals(amount, transactionOutputDTO.getAmount());
        assertEquals(currency, transactionOutputDTO.getCurrency());
        assertEquals(description, transactionOutputDTO.getDescription());
        assertEquals(date, transactionOutputDTO.getDate());
        assertEquals(categoryName, transactionOutputDTO.getCategoryName());
    }
}
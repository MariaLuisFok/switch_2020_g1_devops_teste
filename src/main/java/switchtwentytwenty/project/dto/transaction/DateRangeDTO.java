package switchtwentytwenty.project.dto.transaction;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class DateRangeDTO {
    @Getter
    @Setter
    private String startDate;
    @Getter
    @Setter
    private String endDate;

    public DateRangeDTO(String startDate, String endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }
}

package switchtwentytwenty.project.controllers.icontrollers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import switchtwentytwenty.project.dto.transaction.DateRangeDTO;

public interface IGetAccountTransactionsBetweenDates {
    @GetMapping("/accounts/{accountId}/transactions")
    ResponseEntity<Object> getAccountTransactionsBetweenDates(@PathVariable("accountId") long accountId, @RequestBody DateRangeDTO dateRangeDTO);
}

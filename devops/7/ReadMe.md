#  Task 7 : Maven; cloud deployment of App with Ansible

## Ansible

Ansible is an open-source automation tool, or platform, used for tasks such as configuration management, application deployment, intraservice orchestration and provisioning.

### Ansible playbook

The playbook is the core component of any Ansible configuration. An Ansible playbook contains one or multiple plays, each of which define the work to be done for a configuration on a managed server.

### Implementing Ansible

Ansible can only be installed in Unix type machines so, we will have to use a virtual machine (throught Vagrant) to  be able to execute Ansible.
We will use Vagrant to create the hosts that Ansible will manage/configure.

### Structural changes to the project 

First we need to add the maven wrapper to our project. In order to do that we should run the following command inside maven terminal in our project:

```
mvn -N io.takari:maven:wrapper
```

Now, let's add a pluggin to our _POM.xml_ to generate a _.war_ file instead of the _.jar_ file:

```
<!-- Build an executable WAR -->
            <plugin>
                <artifactId>maven-war-plugin</artifactId>
                <configuration>
                    <attachClasses>true</attachClasses>
                    <webResources>
                        <resource>
                            <directory>src/main/java</directory>
                            <filtering>true</filtering>
                        </resource>
                    </webResources>
                </configuration>
            </plugin>
```

Now, to see if the pluggin is working let's run at project root the following command:

```
./mvnw package -Dmaven.test.skip=true
```

That will assemble our application without running test and the final _.war_ file will be created at /target/ folder.

### Running Jenkins on AWS

To run Jenkins on AWS just follow this [tutorial](https://www.jenkins.io/doc/tutorials/tutorial-for-installing-jenkins-on-AWS/#Create%20a%20key%20pair%20using%20Amazon%20EC2).

After you sign in your AWS cloud, make sure you have git installed. You can do it by running:

```
sudo yum install git -y
```

Now let's start to configure our pipeline!

### Configure

Let's first install Ansible. For that, let's use a Vagrantfile that will set up 3 virtual machines:

```
Vagrant.configure("2") do |config|
  config.vm.box = "envimation/ubuntu-xenial"

  config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get update -y
    sudo apt-get install iputils-ping -y
    sudo apt-get install python3 --yes
  SHELL

  config.vm.define "web" do |web|
    web.vm.box = "envimation/ubuntu-xenial"
    web.vm.hostname = "host1"
    web.vm.network "private_network", ip: "192.168.33.11"

    # We want to access tomcat from the host using port 8080
    host1.vm.network "forwarded_port", guest: 8080, host: 8080
  end

  config.vm.define "db" do |db|
    db.vm.box = "envimation/ubuntu-xenial"
    db.vm.hostname = "host2"
    db.vm.network "private_network", ip: "192.168.33.12"
  end

  config.vm.define "ansible" do |ansible|
    ansible.vm.box = "envimation/ubuntu-xenial"
    ansible.vm.hostname = "ansible"
    ansible.vm.network "private_network", ip: "192.168.33.10"

    # For some Windows and for running ansible "inside" jenkins
    # ansible.vm.synced_folder ".", "/vagrant", mount_options: ["dmode=777,fmode=777"]
    # It seems that ansible has security issues with the previous command. Use instead:
    ansible.vm.synced_folder ".", "/vagrant", mount_options: ["dmode=775,fmode=600"]

    # For acessing jenkins in 8081
    ansible.vm.network "forwarded_port", guest: 8080, host: 8081

    ansible.vm.provision "shell", inline: <<-SHELL
      sudo apt-get install -y --no-install-recommends apt-utils
      sudo apt-get install software-properties-common --yes
      sudo apt-add-repository --yes --u ppa:ansible/ansible
      sudo apt-get install ansible --yes
      # For jenkins
      # sudo apt-get http://mirrors.jenkins.io/war-stable/latest/jenkins.war
      # sudo java -jar jenkins.war
    SHELL
  end
end
```

Now let's run 
```
vagrant up 
```

We can see now that we already have 3 new containers on our VirtualBox.

Let's now enter the VM

```
vagrant ssh ansible
```

and check the version:

```
ansible --version
```

Now let's create the host file to provide Ansible with information about servers by specifying them in an inventory
file.

```
web ansible_ssh_host=192.168.33.11 ansible_ssh_port=22 ansible_ssh_user=vagrant ansible_ssh_private_key_file=/vagrant/.vagrant/machines/web/virtualbox/private_key
db ansible_ssh_host=192.168.33.12 ansible_ssh_port=22 ansible_ssh_user=vagrant ansible_ssh_private_key_file=/vagrant/.vagrant/machines/db/virtualbox/private_key
```

Now, let's test the connection between ansible box and web box using ping command: 

```
ansible web -i hosts -m ping
```

Now let's create a configuration file in the current directory:

```
[defaults]
inventory = /vagrant/hosts
remote_user = vagrant
```

Now we can invoke ansible without passing the "-i" argument that sets the inventory file to use, because it will use the default value in _ansible.cfg_.

Next we create a playbook file for Ansible:

```
---
- hosts: otherservers
  become: yes
  tasks:
    - name: update apt cache
      apt: update_cache=yes
    - name: remove apache
      apt: name=apache2 state=absent
    - name: install jdk
      apt: name=openjdk-8-jdk-headless state=present
- hosts: web
  become: yes
  tasks:
    - name: install tomcat8
      apt: name=tomcat8 state=present

    - name: install base packages
      apt: pkg={{item}} state=present update_cache=yes cache_valid_time=604800
      with_items:
        - git
        - maven
- hosts: db
  become: yes
  tasks:
    - name: install base packages
      apt: pkg={{item}} state=present update_cache=yes cache_valid_time=604800
      with_items:
        - postgresql
---
``` 


Inside this file we set up some ansible defaults

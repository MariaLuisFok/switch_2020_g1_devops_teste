package switchtwentytwenty.project.repositories;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.annotation.Profile;
import switchtwentytwenty.project.applicationservices.implservices.FamilyMemberService;
import switchtwentytwenty.project.datamodel.assembler.PersonDomainDataAssembler;
import switchtwentytwenty.project.datamodel.person.PersonJPA;
import switchtwentytwenty.project.datamodel.shared.*;
import switchtwentytwenty.project.domain.exceptions.ObjectDoesNotExistException;
import switchtwentytwenty.project.domain.model.person.Person;
import switchtwentytwenty.project.domain.model.shared.*;
import switchtwentytwenty.project.dto.family.FamilyInputDTO;
import switchtwentytwenty.project.dto.person.FamilyMemberVOs;
import switchtwentytwenty.project.dto.family.FamilyOutputDTO;
import switchtwentytwenty.project.repositories.irepositories.IPersonRepositoryJPA;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@Profile("PersonRepository")
class PersonRepositoryTest {
    @Mock
    FamilyInputDTO familyInputDTO;
    @Mock
    FamilyMemberService familyMemberService;
    @Mock
    PersonDomainDataAssembler personDomainDataAssembler;
    @Mock
    IPersonRepositoryJPA iPersonRepositoryJPA;
    @InjectMocks
    PersonRepository personRepository;

    @Test
    void getPersonByEmailSuccessfully() {
        //Arrange
        FamilyOutputDTO familyOutputDTO = new FamilyOutputDTO();
        familyOutputDTO.setFamilyId(0);
        familyOutputDTO.setFamilyName("Martins");

        lenient().when(familyMemberService.createFamily(isA(familyInputDTO.getClass()))).thenReturn(familyOutputDTO);

        Email email = new Email("something@gmail.com");
        EmailJPA emailJPA = new EmailJPA("something@gmail.com");

        PersonJPA aPersonJPA = new PersonJPA(new EmailJPA("something@gmail.com"), new PersonNameJPA("David"),
                new AddressJPA("David St", "Porto", "1234-123"),
                new BirthDateJPA("12/01/2000"), new PersonVatJPA("969999999"), 0);

        FamilyMemberVOs familyMemberVOs = new FamilyMemberVOs(new Email("something@gmail.com"), new PersonName("David"), new Address("David St", "Porto", "1234-123"), new BirthDate("12/01/2000"), new PersonVat("123456789"), new FamilyId(familyOutputDTO.getFamilyId()), new ArrayList<>(), new ArrayList<>());

        when(iPersonRepositoryJPA.getByEmail(emailJPA)).thenReturn(Optional.of(aPersonJPA));
        when(personDomainDataAssembler.jpaValueObjectsToDomain(aPersonJPA)).thenReturn(familyMemberVOs);
        List<OtherEmailJPA> otherEmails = new ArrayList<>();
        when(iPersonRepositoryJPA.getOtherEmailsById(emailJPA)).thenReturn(otherEmails);
        List<Email> emails = new ArrayList<>();
        when(personDomainDataAssembler.emailAddressesJPATODomain(otherEmails)).thenReturn(emails);

        //Act
        Person person = personRepository.getByEmail(email);

        //Assert
        assertNotNull(person);
    }

    @Test
    void getPersonByEmailThrowsExceptionWhenPersonDoesNotExist() {
        //Arrange
        Email email = new Email("antonio@gmail.com");
        EmailJPA emailJPA = new EmailJPA("antonio@gmail.com");
        Optional<PersonJPA> personOptionalJPA = Optional.empty();

        when(iPersonRepositoryJPA.getByEmail(emailJPA)).thenReturn(personOptionalJPA);

        //Assert
        assertThrows(ObjectDoesNotExistException.class, () ->
                personRepository.getByEmail(email));
    }
}

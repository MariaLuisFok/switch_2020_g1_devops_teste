import React from "react";
import {useHistory} from "react-router-dom";

function AccountsMain() {

    const history = useHistory();

        let handleClick = () => {
            history.push('/accounts/create-personal-cash');
        }
            return (
                <div align='center'>
                    <h1>Accounts</h1>
                    <br/>
                    <p>Soon list of accounts will be displayed here.</p>
                    <br/>
                    <br/>
                    <button onClick={handleClick} id="button"> Create Personal Cash Account</button>
                </div>
            )
    }
export default AccountsMain;
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
		        git credentialsId: 'ajg-bitbucket-credentials', url: 'https://AnaJorge@bitbucket.org/MariaLuisFok/switch_2020_g1_devops_teste'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Assembling...'
		            sh './mvnw compile'
            }
        }
	    stage('Test') {
            steps {
                echo 'Testing...'
		            sh './mvnw test jacoco:report'
                echo 'Publishing code coverage...'
                    publishCoverage adapters: [jacocoAdapter('target/site/jacoco/jacoco.xml')]
            }
            post {
                always {
                    junit '**/target/surefire-reports/TEST-*.xml'
                }
            }
        }
        stage('Javadoc') {
            steps {
                echo 'Generating Javadoc...'
                    sh './mvnw javadoc:javadoc'

                    publishHTML (target: [
                        allowMissing: true,
                        alwaysLinkToLastBuild: true,
                        keepAll: true,
                        reportDir: 'target/site/apidocs',
                        reportFiles: 'index.html',
                        reportName: 'Javadoc Report',
                    ])
            }
        }
        stage('Install Frontend Dependencies') {
            when {
                changeset "src/main/ui/package.json"
            }
            steps {
                dir('src/main/ui'){
                    script {
                        sh 'yarn install'
                    }
                }
            }
        }
        stage('Assemble Frontend') {
            steps{
                echo 'Assembling Frontend...'
                dir('src/main/ui'){
                    script {
                        sh 'yarn; CI=false yarn run build '
                    }
                }
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                    sh './mvnw clean package' 
                    archiveArtifacts 'target/*.war'
                        dir('src/main/ui'){
                        archiveArtifacts artifacts: '**/build/'
                }
            }
        }
        stage('Publish Web Image') {
            steps {
                echo 'Publishing Web Image...'
                script {
                    docker.withRegistry('https://registry.hub.docker.com', 'ajg-docker-credentials') {
                        def webImage = docker.build("anajorgegraca/project-web", "devops/3/web")
                        webImage.push("web")
                    }
                }
            }
        }
        stage('Publish DB Image') {
            steps {
                echo 'Publishing DB Image...'
                script {
                    docker.withRegistry('https://registry.hub.docker.com', 'ajg-docker-credentials') {
                        def dbImage = docker.build("anajorgegraca/project-db", "devops/3/db")
                        dbImage.push("db")
                    }
                }
            }
        }
        stage('Publish UI Image') {
            steps {
                echo 'Publishing UI Image...'
                script {
                    docker.withRegistry('https://registry.hub.docker.com', 'ajg-docker-credentials') {
                        def dbImage = docker.build("anajorgegraca/project-ui", "src/main/ui")
                        dbImage.push("ui")
                    }
                }
            }
        }
    }
}
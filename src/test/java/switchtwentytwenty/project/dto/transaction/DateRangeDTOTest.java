package switchtwentytwenty.project.dto.transaction;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DateRangeDTOTest {

    @Test
    void createDateRangeDTOSuccessfully() {
        String startDate = "12/01/2020";
        String endDate = "13/01/2020";
        DateRangeDTO dateRangeDTO = new DateRangeDTO(startDate, endDate);

        assertNotNull(dateRangeDTO);
        assertEquals(startDate, dateRangeDTO.getStartDate());
        assertEquals(endDate, dateRangeDTO.getEndDate());
    }

    @Test
    void setStartDate() {
        String startDate = "12/01/2020";
        String endDate = "13/01/2020";
        DateRangeDTO dateRangeDTO = new DateRangeDTO();
        dateRangeDTO.setStartDate(startDate);
        dateRangeDTO.setEndDate(endDate);

        assertNotNull(dateRangeDTO);
        assertEquals(startDate, dateRangeDTO.getStartDate());
        assertEquals(endDate, dateRangeDTO.getEndDate());
    }
}
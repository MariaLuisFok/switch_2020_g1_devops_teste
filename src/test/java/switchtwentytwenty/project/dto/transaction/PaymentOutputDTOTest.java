package switchtwentytwenty.project.dto.transaction;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PaymentOutputDTOTest {
/*
    @Test
    void createPaymentOutputDTOWithNoArguments() {
        PaymentOutputDTO paymentOutputDTO = new PaymentOutputDTO();

        assertNotNull(paymentOutputDTO);
    }

    @Test
    void createPaymentOutputDTOWithAllArguments() {
        long accountId = 4;
        double amount = 12.67;
        String currency = "GBP";
        String description = "Phone bill";
        String date = "12-09-2010 13:33";
        String destinationEntity = "Vodafone";
        Object categoryId = 2;
        long transactionId = 1;
        String categoryName = "Bills";
        double balance = 200.50;
        PaymentOutputDTO paymentOutputDTO = new PaymentOutputDTO(transactionId, accountId, amount, currency, description, date, destinationEntity, categoryId, categoryName, balance);

        assertNotNull(paymentOutputDTO);
    }

    @Test
    void createPaymentOutputDTOWithoutTransactionId() {
        long accountId = 4;
        double amount = 12.67;
        String currency = "GBP";
        String description = "Phone bill";
        String date = "12-09-2010 13:33";
        String destinationEntity = "Vodafone";
        Object categoryId = 2;
        String categoryName = "Bills";
        double balance = 200.50;
        PaymentOutputDTO paymentOutputDTO = new PaymentOutputDTO(accountId, amount, currency, description, date, destinationEntity, categoryId, categoryName, balance);

        assertNotNull(paymentOutputDTO);
    }

    @Test
    void ensureDTOAttributesAreValid() {
        long accountId = 4;
        double amount = 12.67;
        String currency = "GBP";
        String description = "Phone bill";
        String date = "12-09-2010 13:33";
        String destinationEntity = "Vodafone";
        Object categoryId = 2;
        long transactionId = 1;
        String categoryName = "Bills";
        double balance = 200.50;
        PaymentOutputDTO paymentOutputDTO = new PaymentOutputDTO(transactionId, accountId, amount, currency, description, date, destinationEntity, categoryId, categoryName, balance);

        long resultAccountId = paymentOutputDTO.getAccountId();
        double resultAmount = paymentOutputDTO.getAmount();
        String resultCurrency = paymentOutputDTO.getCurrency();
        String resultDescription = paymentOutputDTO.getDescription();
        String resultDate = paymentOutputDTO.getDate();
        String resultDestinationEntity = paymentOutputDTO.getDestinationEntity();
        Object resultCategoryId = paymentOutputDTO.getCategoryId();
        long resultTransactionId = paymentOutputDTO.getTransactionId();

        assertNotNull(paymentOutputDTO);
        assertEquals(accountId, resultAccountId);
        assertEquals(amount, resultAmount);
        assertEquals(currency, resultCurrency);
        assertEquals(description, resultDescription);
        assertEquals(date, resultDate);
        assertEquals(categoryId, resultCategoryId);
        assertEquals(destinationEntity, resultDestinationEntity);
        assertEquals(transactionId, resultTransactionId);
    }

    @Test
    void ensureDTOAttributesAreSettable() {
        long accountId = 4;
        double amount = 12.67;
        String currency = "GBP";
        String description = "Phone bill";
        String date = "12-09-2010 13:33";
        String destinationEntity = "Vodafone";
        Object categoryId = 2;
        long transactionId = 1;
        String categoryName = "Category";
        double balance = 124.32;

        PaymentOutputDTO paymentOutputDTO = new PaymentOutputDTO();

        paymentOutputDTO.setAccountId(accountId);
        paymentOutputDTO.setAmount(amount);
        paymentOutputDTO.setCurrency(currency);
        paymentOutputDTO.setDescription(description);
        paymentOutputDTO.setDate(date);
        paymentOutputDTO.setDestinationEntity(destinationEntity);
        paymentOutputDTO.setCategoryId(categoryId);
        paymentOutputDTO.setTransactionId(transactionId);
        paymentOutputDTO.setCategoryName(categoryName);
        paymentOutputDTO.setBalance(balance);

        assertNotNull(paymentOutputDTO);
        assertEquals(accountId, paymentOutputDTO.getAccountId());
        assertEquals(amount, paymentOutputDTO.getAmount());
        assertEquals(currency, paymentOutputDTO.getCurrency());
        assertEquals(description, paymentOutputDTO.getDescription());
        assertEquals(date, paymentOutputDTO.getDate());
        assertEquals(categoryId, paymentOutputDTO.getCategoryId());
        assertEquals(destinationEntity, paymentOutputDTO.getDestinationEntity());
        assertEquals(categoryName, paymentOutputDTO.getCategoryName());
        assertEquals(balance, paymentOutputDTO.getBalance());
    }


    @Test
    void equalsAndHashCode() {
        long accountId1 = 4;
        double amount1 = 12.67;
        String currency1 = "GBP";
        String description1 = "Phone bill";
        String date1 = "12-09-2010 13:33";
        String destinationEntity1 = "Vodafone";
        Object categoryId1 = 2;
        long transactionId1 = 1;
        String categoryName = "Bills";
        double balance = 200.50;

        long accountId2 = 5;
        double amount2 = 12.68;
        String description2 = "Shopping";

        long transactionId2 = 7;

        //act
        PaymentOutputDTO outputDTO = new PaymentOutputDTO(transactionId1, accountId1, amount1, currency1, description1, date1, destinationEntity1, categoryId1, categoryName, balance);
        PaymentOutputDTO outputDTOEqual = outputDTO;
        PaymentOutputDTO outputDTONull = null;
        PaymentOutputDTO outputDTOSame = new PaymentOutputDTO(transactionId1, accountId1, amount1, currency1, description1, date1, destinationEntity1, categoryId1, categoryName, balance);
        PaymentOutputDTO outputDTODifferentId = new PaymentOutputDTO(transactionId2, accountId1, amount1, currency1, description1, date1, destinationEntity1, categoryId1, categoryName, balance);
        PaymentOutputDTO outputDTODifferentAccountId = new PaymentOutputDTO(transactionId1, accountId2, amount1, currency1, description1, date1, destinationEntity1, categoryId1, categoryName, balance);
        PaymentOutputDTO outputDTODifferentAmount = new PaymentOutputDTO(transactionId1, accountId1, amount2, currency1, description1, date1, destinationEntity1, categoryId1, categoryName, balance);
        PaymentOutputDTO outputDTODifferentDescription = new PaymentOutputDTO(transactionId1, accountId1, amount2, currency1, description2, date1, destinationEntity1, categoryId1, categoryName, balance);
        PaymentInputDTO paymentInputDTO = new PaymentInputDTO(amount1, 3, description1, date1, destinationEntity1, categoryId1);

        //assert
        assertEquals(outputDTO, outputDTOSame);
        assertNotSame(outputDTO, outputDTOSame);
        assertEquals(outputDTO, outputDTOEqual);
        assertEquals(outputDTO.hashCode(), outputDTOSame.hashCode());
        assertNotEquals(outputDTO, outputDTONull);
        assertFalse(outputDTO.equals(transactionId1));
        assertFalse(outputDTO.equals(accountId1));
        assertFalse(outputDTO.equals(amount1));
        assertFalse(outputDTO.equals(currency1));
        assertEquals(outputDTO, outputDTODifferentId);
        assertNotEquals(outputDTO, outputDTODifferentAccountId);
        assertNotEquals(outputDTO, outputDTODifferentAmount);
        assertNotEquals(outputDTO, outputDTODifferentDescription);
        assertTrue(outputDTO.equals(outputDTODifferentId));
        assertFalse(outputDTO.equals(outputDTODifferentAccountId));
        assertFalse(outputDTO.equals(outputDTODifferentAmount));
        assertFalse(outputDTO.equals(outputDTODifferentDescription));
        assertNotEquals(outputDTO.hashCode(), outputDTODifferentId.hashCode());
        assertNotEquals(outputDTO.hashCode(), outputDTODifferentAccountId.hashCode());
        assertNotEquals(outputDTO.hashCode(), outputDTODifferentAmount.hashCode());
        assertNotEquals(outputDTO.hashCode(), outputDTODifferentDescription.hashCode());
        assertNotEquals(0, outputDTO.hashCode());
        assertNotEquals(outputDTO, accountId2);
        assertNotEquals(outputDTO, amount2);
        assertNotEquals(outputDTO, paymentInputDTO);
        assertFalse(outputDTO.equals(paymentInputDTO));
    }*/

}

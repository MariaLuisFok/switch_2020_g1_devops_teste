package switchtwentytwenty.project.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import switchtwentytwenty.project.applicationservices.iservices.ITransactionService;
import switchtwentytwenty.project.controllers.icontrollers.IRegisterPaymentController;
import switchtwentytwenty.project.controllers.icontrollers.ITransferController;
import switchtwentytwenty.project.dto.transaction.*;

import java.time.DateTimeException;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@RestController
public class TransactionController implements IRegisterPaymentController, ITransferController, switchtwentytwenty.project.controllers.icontrollers.IGetAccountTransactionsBetweenDates {
    @Autowired
    private ITransactionService transactionService;

    @Override
    @PostMapping("/transactions/{accountId}/payments")
    public ResponseEntity<Object> registerPayment(@RequestBody PaymentInputDTO paymentInputDTO, @PathVariable long accountId) {
        TransactionOutputDTO transactionOutputDTO;
        try {
            transactionOutputDTO = transactionService.registerPayment(paymentInputDTO, accountId);
            long transactionId = transactionOutputDTO.getTransactionId();
            Link selfLink =
                    linkTo(TransactionController.class).slash("transactions").slash(transactionId).withRel("paymentInformation");
            Link accountOptions = linkTo(AccountController.class).slash("accounts").slash("accountId").withRel("accountOptions");
            transactionOutputDTO.add(selfLink, accountOptions);
            return new ResponseEntity<>(transactionOutputDTO, HttpStatus.CREATED);
        } catch (IllegalArgumentException | DateTimeException | NullPointerException exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @PostMapping("/transactions/{originAccountId}/transfers")
    public ResponseEntity<Object> transfer(@RequestBody TransferInputDTO transferInputDTO, long destinationAccountId, @PathVariable long originAccountId) {
        try {
            TransactionOutputDTO transferOutputDTO = transactionService.transfer(transferInputDTO, originAccountId, destinationAccountId);
            long transactionId = transferOutputDTO.getTransactionId();
            Link selfLink = linkTo(TransactionController.class).slash("transactions").slash(transactionId).withRel("transferInformation");
            Link accountOptions = linkTo(AccountController.class).slash("accounts").slash("accountId").withRel("accountOptions");
            transferOutputDTO.add(selfLink, accountOptions);
            return new ResponseEntity<>(transferOutputDTO, HttpStatus.CREATED);
        } catch (IllegalArgumentException | DateTimeException | NullPointerException exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    @GetMapping("/accounts/{accountId}/transactions")
    public ResponseEntity<Object> getAccountTransactionsBetweenDates(@PathVariable("accountId") long accountId, @RequestBody DateRangeDTO dateRangeDTO) {
        try {
            TransactionListDTO transactionListDTO = transactionService.getAccountTransactionsBetweenDates(accountId, dateRangeDTO);
//            for (TransactionOutputDTO transactionOutputDTO : transactionListDTO.getTransactionList()) {
//            }
            return new ResponseEntity<>(transactionListDTO, HttpStatus.OK);
        } catch (Exception exception) {
            return new ResponseEntity<>(exception.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}

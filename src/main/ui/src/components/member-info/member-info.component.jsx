import React from "react";
import { Link } from "react-router-dom";

import { withStyles } from "@material-ui/core";

import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import Chip from "@material-ui/core/Chip";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBirthdayCake,
  faHouseUser,
  faHome,
  faFileInvoiceDollar,
  faMobileAlt,
  faAt,
} from "@fortawesome/free-solid-svg-icons";

import InfoSection from "./member-info-section.component";

import styles from "./member-info.styles";

// from https://stackoverflow.com/a/63763497
function getAvatarLetters(text) {
  return text
    .match(/(^\S\S?|\b\S)?/g)
    .join("")
    .match(/(^\S|\S$)?/g)
    .join("")
    .toUpperCase();
}

const MemberInfo = ({
  name,
  mainEmail,
  admin,
  birthDate,
  familyName,
  address,
  vat,
  phoneNumbers,
  emailAddresses,
  classes,
}) => {
  return (
    <Card>
      <CardContent>
        <div className={classes.cardHeader}>
          <Avatar className={classes.avatar}>{getAvatarLetters(name)}</Avatar>
          <Typography variant="h5" component="h2">
            {name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {mainEmail}
          </Typography>
          {admin && (
            <Chip
              className={classes.chip}
              color="primary"
              size="small"
              label="Family Admin"
              // onClick={handleClick}
            />
          )}
        </div>
        <Box display={"flex"} className={classes.infoSection}>
          <Box flex={1}>
            <InfoSection
              icon={faBirthdayCake}
              label={"Birth Date"}
              text={birthDate}
            />
          </Box>
          <Box flex={1}>
            <InfoSection
              icon={faHouseUser}
              label={"Family Name"}
              text={familyName}
            />
          </Box>
        </Box>
        <Box display={"flex"} className={classes.infoSection}>
          <Box flex={"auto"}>
            <InfoSection icon={faHome} label={"Address"} text={address} />
          </Box>
        </Box>
        <Box display={"flex"} className={classes.infoSection}>
          <Box flex={"auto"}>
            <InfoSection
              icon={faFileInvoiceDollar}
              label={"VAT Number"}
              text={vat}
            />
          </Box>
        </Box>
        <Box display={"flex"} className={classes.infoSection}>
          {phoneNumbers.length > 0 && (
            <Box flex={1}>
              <Typography variant="body2" color="textSecondary" component="p">
                <FontAwesomeIcon icon={faMobileAlt} /> Phone Numbers
              </Typography>
              {phoneNumbers.map((phone) => (
                <Typography key={phone} variant="body2" component="p">
                  {phone}
                </Typography>
              ))}
            </Box>
          )}
          {emailAddresses.length > 0 && (
            <Box flex={1}>
              <Typography variant="body2" color="textSecondary" component="p">
                <FontAwesomeIcon icon={faAt} /> Other Email Addresses
              </Typography>
              {emailAddresses.map((email) => (
                <Typography key={email} variant="body2" component="p">
                  {email}
                </Typography>
              ))}
            </Box>
          )}
        </Box>
      </CardContent>
      <CardActions>
        <Button component={Link} to={"/profile/add-email"} size="small">
          Add Email
        </Button>
      </CardActions>
    </Card>
  );
};

export default withStyles(styles)(MemberInfo);

#  Apply DevOps to the Personal Finance Project

The goal of this assignment is to implement a pipeline in Jenkins for our Personoal Finance project following the next concerns:

- *Infrastructure as Code* : It should be possible to reproduce your solution in a system by using a clone of your repository
- *Git/Bitbucket* : create tasks and issues for all team members and use branches to develop specific features of the solution;
- *Hosts* : 3 hosts 
    - *Control* host : with Jenkins and Ansible
    - *Web* host : to install and configure the Personal Finance web application
    - *Db* host : One host to install and configure the database of the application
- *Build Tools* : Maven and/or Gradle
- *Build Stages of the Pipeline* :
    - assemble the application;
    - generate and publish javadoc in Jenkins;
    - execute tests (unit and integration) and publish its results in Jenkins, including code coverage;
    - publish the distributable artifacts in Jenkins;
    - integration with Docker: build 2 docker images (app + db) and publish them in docker hub;
    - integration with Ansible: use Ansible to deploy and configure the application and its database.

## Tasks

For this assignment we will have 8 major tracks:

1. Maven; local deployment of App with Ansible to Vagrant VMs
2. Gradle; local deployment of App with Ansible to Vagrant VMs
3. Maven; local smoke test of App with Docker containers; publish docker images; manually execute App in local containers
4. Gradle; local smoke test of App with Docker containers; publish docker images; manually execute App in local containers
5. Maven; local smoke test of App with Docker containers; publish docker images; manually execute App in cloud containers
6. Gradle; local smoke test of App with Docker containers; publish docker images; manually execute App in cloud containers
7. Maven; cloud deployment of App with Ansible
8. Gradle; cloud deployment of App with Ansible

### Build Tools

#### Maven

Maven is a building tool that can be used for building and managing any Java-based project. The primary goals are:

- Making the build process easy;
- Providing a uniform build system;
- Providing quality project information;
- Encouraging better development practices.

Maven is a lot more verbose in comparison with Gradle. Using a markup language as XML to describe configurations or new tasks is not very __user-friendly__ when compared with a scripting type as we saw with Gradle.

Maven has less performance compared with Gradle, [source](https://gradle.org/maven-vs-gradle/), and that is something that has a significant importance to ship software faster.

#### Gradle 

Gradle is an open-source build automation tool that is designed to be flexible enough to build almost any type of software. 

The core model of gradle is based on tasks, that means that a build essentially configures a set of tasks and wires them together - based on their dependencies.

The following is a high-level overview of some of its most important features:

- High performance : it uses a build cache to enable the reuse of task outputs from previous runs.
- JVM foundation 
- Conventions : you can easily end up with slim build scripts for many projects using the right pluggins. But these conventions don’t limit you: Gradle allows you to override them, add your own tasks, and make many other customizations to your convention-based builds.
- Extensibility: you can easily extend Gradle to provide your own task typer or even build model.
- IDE support. 

## References 

https://gradle.org

package switchtwentytwenty.project.controllers.icontrollers;

import org.springframework.http.ResponseEntity;

public interface IGetProfileInformationController {
    ResponseEntity<Object> getProfileInformation(String personId);
}
